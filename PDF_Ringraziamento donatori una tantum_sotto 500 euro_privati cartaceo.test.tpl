{literal}
    <style>
        @font-face {
          font-family: 'Anonymous Pro';
          font-style: normal;
          font-weight: normal;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/Anonymous-Pro-Regular.woff) format("woff");
        }
        @font-face {
          font-family: 'Montserrat';
          font-style: normal;
          font-weight: 700;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/Montserrat-Bold.ttf) format('truetype');
        }
        @font-face {
          font-family: 'Roboto Slab';
          font-style: normal;
          font-weight: normal;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/RobotoSlab-Regular.woff) format("woff");
        }
        @font-face {
          font-family: 'Roboto Slab';
          font-style: normal;
          font-weight: 700;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/RobotoSlab-Bold.woff) format("woff");
        }

        @media print,
        dompdf {

            @page {
                size: 210mm 297mm;
                margin: 0;
            }

            body {
                font-size: 8pt;
                font-family: 'Roboto Slab', serif;
                font-weight: inherit;
            }

            p, table {
                font-family: 'Roboto Slab', serif;
                font-weight: inherit;
                line-height: 8pt;
            }

            .letter-header {
                margin-left:-17mm;
                margin-top:-10mm;
            }

            .letter-footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }

            .letter-footer td {
                text-align: left;
                font-family: 'Roboto Slab', serif;
                font-weight: inherit;
            }
        }
    </style>
{/literal}
<div class="letter-body">
    <p class="letter-header">
        <img style="width: 210mm;" src="https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/contribute/images/uploads/header-202311.png">
        <br>
    </p>
    <p>Milano, {$smarty.now|date_format:"%d/%m/%Y"}
        <br>
    </p>
    <p>
        Gentile<strong> {contact.first_name}</strong>,<br>grazie per aver scelto di sostenere la conoscenza libera.<br></p>
    <p><b>Insieme a te ci impegniamo per far crescere in Italia Wikipedia e i progetti Wikimedia, oltre che OpenStreetMap</b>, in linea con un più ampio impegno a promuovere la cultura della condivisione della conoscenza. 
Solo grazie al tuo contributo possiamo concretizzare la nostra missione di <b>mantenere internet uno spazio aperto, gratuito e di libera partecipazione</b>. </p><p>Tutto questo richiede un grande lavoro di squadra e siamo lieti che tu, {contact.first_name}, abbia deciso di farne parte. 
</p>

    <p>A presto,</p>
    <p>Francesca Lissoni<br>Coordinatrice Raccolta fondi Wikimedia Italia</p>
    <p>P.S. Stiamo facendo delle scelte importanti per ridurre il nostro impatto sull'ambiente, comunicaci a questo indirizzo la tua email:<strong> segreteria@wikimedia.it</strong>.</p>


    <table class="table table-bordered">
        <tbody>
            <tr>
                <td width="70%">
                    <p>Oppure, iscriviti alla nostra newsletter mensile tramite QRCode o visitando la pagina
                        <a href="https://sostieni.wikimedia.it/newsletter" target="_blank">https://sostieni.wikimedia.it/newsletter</a>.</p>
                    
                    <p>
                        <b>Il riepilogo della tua donazione:</b>
                    </p>
                    <p>Importo: {contribution.total_amount}<br>Data: {contribution.receive_date|crmDate}</p>
                </td>
                <td>
                    <img style="width: 25%;" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAYEBQYFBAYGBQYHBwYIChAKCgkJChQODwwQFxQYGBcUFhYaHSUfGhsjHBYWICwgIyYnKSopGR8tMC0oMCUoKSj/wAALCACWAJYBAREA/8QAHAAAAgMBAQEBAAAAAAAAAAAAAAcFBggEAwIB/8QATxAAAQMDAwEDAhEICAQHAAAAAQIDBAUGEQAHEiEIEzEUQRYXGCIjMjZRVmFxdHWRk5SzFTdSVYGh0dIkNDhzlbGy0zVicsMlM0JERoKS/9oACAEBAAA/ANU6NGjRo0aNGjRo0aNGjRo0aNctVqMSk06RPqUhuNDjoLjrzhwlCR4knVO9N6wPhbSPtxo9N6wPhbSPtxo9N6wPhbSPtxo9N6wPhbSPt9WOq3PRaTQm61UqlGjUpwIUiU4vCFBftSD8eq/G3asSTIaYj3VS3HnVhCEJd6qUTgAdPf1eNR9erNOoFLeqVamMwoLPHvH3lYSnJCRk/GSB+3XjbNyUe54C5tv1GPUIqHC0p1hXJIWACU/Lgj69ed0XVQ7Wjsv3DU41OZeWW21vr4hSsZwP2a+vRPRfQ16IDUowovd975YV+x8c4zn3s9NVz03rA+FtI+3Gj03rA+FtI+3Gj03rA+FtI+3Gj03rA+FtI+31d47zcmO2+wtLjTiQtC0nIUkjII/Zr00aom+v5oLu+j3dZU2D2cgbmUirTJ1WlwVw5CWUpZbSoKBTnJzppepPovwlqf2Dej1J9F+EtT+wb0ne0BtVC2xeobcCpyp35QS8pffISnhw4Yxj3+R+rWqahZLF/wCytAoUqY7DacgwnS60gKUODaTjB1QaR2W6TTqpDnIuSoLVGfQ8EqjoAUUqCsePxa0X4aVHal/MfcWM+Mf8dvVc7F/5q5/0s7+E1qJ7bXuRt35+v8M66Xv7Gg+iU/jDSl2F2Xp+5du1GpTqvMhORpfk4Qy2lQUOCVZ6+f12mb6k+i/CWp/d29HqT6L8Jan9g3pMb+7Xw9sqjSI0GpSZwmsuOKLyEp48VAYGPl1ua0vctR/mbP4adSujVE31/NBd30e7pTdiP3L3N89a/D1l+v1mportRSmozQBJdAAfX+mfj1M7WVepO7l2m27UJi0KqsUKSX1kEd6noeunX25P65Z393L/AM2tW7e592P2YaO4w6404I9OwpCik+1T5xqpdnneqgW5akG3a3+VX6q/OUErQ2HEeyKSE5UVg/u1LdtqXIi060jFkPMlT0kHu1lOfWt+ODpZUTYrcK57cgVOPUITkCoMIkNofnrzxUAock8SM6ZO3l30/s/0Z60r7RJcqr75qKDTkh5vulpSgZUopPLLaumPe69dUntHbu27uLQaRDoDdRQ9FlKec8pZSgcSgp6EKPXOmY9/Y0H0Sn8Ya+OxN7g699J/9pGsmTK1VEy3wKlNwHFf+4X75+PVt2Yq1Re3WtJt6fLW2qpsBSVPqII5DxGdNftvf8etb5q//rTrUlpe5aj/ADNn8NOpXRqib6/mgu76Pd0puxH7l7m+etfh6zxXdvbydrVQcatOvLQqQ4UqFPdII5nqPW6l9srCu6HuLa8mXa9cYjs1SM4465BdSlCQ6klRJTgAAeOmp25P65Z393L/AM2tXbeClz6v2aaNEpMGVOlKjU5QZjNKcWQEpyeKQTrHqqfWbfr8Vl+nzIVWacbdaYfjqS5yyCg8FDJycY6ddMG/HN277bhtXRQq7MTDUtTIFHU1xKsBXtUDPtR461Rtjetr0Tby26XWbipECpQ6eyxJiyZaG3WHEoAUhaFEFKgQQQeo1mjtYVim1/c+LJoU+JUo/wCTWW+8iOpdTz5uetyknr1HT49K+s2xXqEy27W6JUqc04vghcuKtoKUOpAKgMnGtZvf2NB9Ep/GGvjsTe4OvfSf/aRrM0zbq9FS31JtKvkFaiCKe716n/l1adoLEuyBuha0qdbFbjxmaiytx52C4lCEhXUklOAPj0we29/x61vmr/8ArTrUlpe5aj/M2fw06ldGqJvt+aC7vo9zWRNld53NsaZU4bdDRUvLH0vc1Su64YTjGOJzpjeq2k/A5n/Ej/t6PVbSfgcz/iR/29Kve7dhzdF6juO0dFM/J6XUgJk993nPj/yjGOP79aur18L2+2NoNeagpnqbhQWu5U73YPNtIzywfD5NKhFrnd5h3dpyZ+SXIGV/kxLffpX5KOX/AJuU45Yx7Xp8emdsTvE7uhNrDDtFRTRAbbcBTJL3PmVDHtRjHHWZJ1ppvjtH123VzTCE2rzR34b7wo4lxftcjPtceOo3dTb9O29/0+iIqKqiFtMye+LPdY5OKHHGT+j458+n522vchbvjj8oL/DOul/+xoPolP4w0ldl97HdsqFPprdCbqQlSvKe8VL7rj6wJxjic+1zn49MH1W0n4HM/wCJH/b0eq2k/A5n/Ej/ALelPvXumvc+dS5LlJRTDBacb4pkd9z5EHPtRjw1vK0fctR/mbP4adS2jUPeNRplJtipT6+2l2lR2VLkoU13oUjzgp8/yaSPpvbJ/qiH/gaf5dHpvbJ/qiH/AIGn+XTKsqFYl5W9HrVDt6kOQHytKFOUxtCiUqKT0KffB1C7i1jbPb5yC3ctBpjSpqVqaDVJbcyEY5Zwnp7Yan7vua06Zt7FrNfitPW26hhTTS4YdSErALfsZHTHT5NIabb1d3Au+LcG2AUxYxeZacjNviG2ooI77LGQDkePTr8er3vttrXajGow2uhRqU6hbvlioLqYJWkhPAKKePLBCse9n49WelxaJtttzTa/d9NiNVWnxWUzpzUZL0gvKwhSu8A5KJUrqrPXJ1lHtD3hSr+3Fh1K2XnnI4hNRgp1stEOBaz4H/qHXU9U9jd26q2huqqVNbbVySmTVg4En3wFKODp07XXpbVMpVE2uuJCnbgYT5BJiLjd6wXASrBV1SRjTV9BdrYz6GqJ9wa/l0mlbubKJUpJo8PIOD/4Gn+XXdQNyNnq7W4FJp1FgrmTXksMpVRUJBWo4GTx6antxK7tjYEqJGuSg01pyW2pxsNUltwFKTg5wnp46aFLfYk02K/DSExnWkLaATxwgpBHTzdMdNdOjUZc9Dh3JQJ9HqQcMOa0WXQ2ririfePm1iTtL7e0Pb+u0eJbqJKWpUZbrnfulwlQXgY6dOmlHBgS6hILMCK/JewVcGWytWB4nAGcac2zu5942xWrZspIai05VQaZcYkRMOhLroKupwRnkcH5Nac3bsiyrufpir2nJirjJcTHBmpj8gop5ePj4DWTb73Dum4402xYndzqJDfLEVuNG7x1TTCiGzyTkq9akZPn1L7TXpuNaaaXb9NpclqkrmpLne0xaiAtaQs8iOnTW4xpXdpqLIm7L1+PDYekPrMfi20grUfZ0HoB10i9itsbMq1suS79edplabnKS0xImCKothKClXBWCQVFQz58fFpwdpm/67YFvUeXbjrDb0mUppzvmQ4CkIJ6A+HXWadoa5NuTtCUKsVQoVNmTy66UJ4pKihXgPN4afnaG3Avu0rnp0OzYqn4TsIOukQC/hzmoe2A6dAOn8dSDHZssF9lDrjFUC1pClf0wjqRk+bXDcmyFsWTQp1z2tGqSq7SGjMghT5dBeR1TlGPXdR4agdu7ck74sz5m60Sa3KpakMQ+5aVDyhYKlZGPXdQNaUp8RqBBjxGOXcsNpaRyOTxSABk/INe+jUFfVwptS0atXVxjKTAYU+WQvgV482cHH1awvvtuezubV6ZMYpblOENhTJQt8O8sqznIAxqO2Wv5vbm8V1t6AuelUZyP3KHg2fXFJznB/R1Nu3gi++0PQ7hahrhIl1SAAwpzvCngptHtsDOeOfDWnN+doJG58miux6uzThT0upUHGC5z5lJ6YIxjj+/WfOzRT1UntBinKcDqonlscuAYCuCVJzjzZxrcWPl0s9691mtr49JdepDlSFQW6gBD4a4cAk+dJznl+7Sr9VtD+CEj7+n+TUfNtd3tAuqv+JJRQ2aenyEwnkGQpZZ9lKuY4gA95jGPN8evWfXfVNBFBgxvQ6uk5nqeeX5SHAfWcQAE49tnPXS420t1Vp9pSk0JySmUuBUiyXko4heEK64yca3iPAeOqHvLuM3tpbcWrvU1dRS/LTF7tDwaKcoWrlkg/oYx8elrZ3aai3LdVIoiLXfjqqEpuMHjNSoI5HHLHAZx8utDjRo0aqO7lInV3ba46XSWDInSoa2mWgoJ5qPgMkgD9p1nDbGi2rtnEnQ97qPT41QmOJegiVFE0loAhRBbCwkcvMcE+8dKS+9tbotmCuuVSkeSUWQ/wAY7oeaUCF8lIASlRIyke900w7Dufbal7LOsz0QWr8bjylRpAgrL7b/ACWWVJeCcBQ9Zg56YHhjVaseZu/fKJqrWrlwzxDKA/iqlvgVZ4+3WM54nw97Tu3it9iy9mI1dpkFmkXehMRMmpRAG5RdUAHsup6kqPLJz1z59KiwF7uVuNEuRisV6TbkaSFynlVXoG21BTuUFfIgJz0x18BnUr2qdxrXvqDbrdrVMzVxHX1PAsON8QoI4+3SM+B08dqNubNqO2lrTJ1rUWRKfprDjrzsNClLUUAlRJHUk6oe71iXvTrsaG2EORS7TEZC5TFNloisqc5K7xRb5JySgJBOOoAHXTI2nru2NWq05rbmPT2p6GAp8xqeqOS3yAGSUjIzjprPG9lg3zSL+ue94EJ+HTGZRktVBmU2haEnCQoAK5jqceGdLP0zb4+F9f8Av7v8dNjs1VGbuDfcylX1MkXFTWae5Ibi1RwyWkOhxtIWErJAUApQz7yjr63N2WvVG6FSqlg2+IdMQ825BchSGY4bIbTkoHIFJ5A+YddUu96nu1ZD8Vi6K/cEF2UhS2k/lUuckg4J9Ys4663Varrj9s0l15anHXIjKlrUclRKASSff1KaNfLriGm1LdWlCEjJUo4A/brIPbO/p9028qD/AElKYTgJZ9eB7J58Z0u9wN2Lru60o1BrsGExTo7ja0LajLbWVISUpyVKI8CfNpYE6YW1G5lybfN1NNsxIcgTS2XvKGFu8eHLjjioY9sfHWj+0RWk1ns8Rpb70fy2SIL7zTagOK1YUoBOcjBJ6aztaG8dx2tZUm1qaxTFU2QHgtTzKlOeyjCsELA+Tp9epzs37ZUXcmfXGa89PaRCaZW15I4lBJUpQOeSVfojTV2x3NrkLd+Jtk03BNvU1+RTWXFNK8oLTCF8CpXLiVesGTxHn6DXb2gN4Lis2/Y1u0lmmrgS4Ta3FPsqU4C4taFYIUB4AebTF2v2dt7biqTJ9Ck1N16SyGFiW6haQkKCsjihPXI1brztuHdtsVChVJb7cSagNuKYUErA5A9CQR4gebSWk9mbb2MrjIq9ZaURkBcxlJP1t6V/Y0SEbsVVKfBNLeA+2a1cN3d8r3tXcWtUSjU+nOwIi0JaW7EcWogtpUckLAPUnza57NhR9/2pc/cx5VMk0gpjxUwCIwWlzKlFQc5ZIKR4Y1qKlxWoNNixI6lKZYaQ0hSjklKUgAk/INdOjVb3Ht9+6bGrdEiOtMvzoymUOO54pJ85x11S+z7tlUdtKVV4tTnRJapr6HUGMFAJASRg8gNLm7bwZ3+S5YdCiu0qdGkGaqTNUFNlLXJBSAjJySsH9mkDce3c6h7ns2S/NjOTXJEeOJCAruwXgkg9RnA5jPTTxtp49mRUli5U/lk1/itk088e6DGQrlzx496MY946zVck9FVuGq1FpC225cp2QlKupSFrKgCff66Z21+w9Y3AtdFcp1Wp0VhTy2e7fSsqykjJ6Ajz6YltxHOzK89MuTjWUV1IZZTTzxLZaPIlXMDx5jGPeOvCJbUm1K96eUt9iRRJDq6mKa1kSQiVlKE5I45SXQT18xx5te1TtKX2hayze9vPsUmHD4U9UedlTilNnvCocARghwD9h08d3tzIW2lLgTahAkzUS3yylLCkgpISVZPLStHavoB/+O1b7Rv+OkXv5uNC3KuWn1OnwJMNEaIIykPlKiTzUrIx5vXa8dhtwYe3F3y6vUIUmY07CXFDbCkhQJWhWevm9Yfr0+vVYUD4O1b7Vv8AjpK9oDc6BuZU6RKp9PlQkwmHGlCQpKioqUCCMfJrcdne5Oi/MmPw06l9GuOs1SHRqXKqNUkJjwoyC486rOEJHiTjrqjenbt18KoP/wCXP5dVW3rs2Qt2ruVSiT6NDnrSpCn2ku8iFHKh1HnIGioXZshUblTcE2fRXqylxt0S1Id5haMcD4YyOI+rXvdl77L3auMu5KpSKiqMFBkvJdPAKxnGB58D6tQG8O0VvT9tDK25teM7UpC2HY7kQHkppRySOR8CkjVx7M1vVa2NsmqdX4LsGaJbyy07jPEkYPQ6ld3HNvkM0v0yhBLZU55H5UlZ64Tzxx/+vj8WsX7i37VZs2t2/Sq5Ids5MlbUKGhXsIjoXlpKQRniAE4z7w1pbsX/AJq5/wBLO/hNaiu237kbd+fr/DOurbWxNum9mKLcl2UWnACGHZUt5Kz4rIycH5B4as1pWNs/d8KRLtui0ifHYX3Ti20OAJVjOOuPMRrO3Zftii3PuTVIFwU5ifDbp7rqGnc4SoOtgEYPvEj9uuO+qTbVA7SDtOlxY0S2I9QjB9gg92lkttleR446k/t189oVdhLqdH9LcQRGDDnlXkqVgc+Q455fFnW4LO9ydF+ZMfhp1L6NUTfX80F3fR7uss9nzZ6kbl0ery6rUahEchyENITG4YIKc5PJJ66a/qUrX/X1c+tr+TR6lK1/19XPra/k0mO0PtZTNsn6E3SZ86WKgh8ueVcPW8OGMcQP0jraO3vWwraz+rI34SdKHc/eut2luxFtWDTqa9CeVGBdeDneDvSArwUB5+nT69Xvd7a6m7msUtqqz5kNMBbikeTcPXcwkHPIH9H9+sd21t7T6tvq9Y702UinonSookI496UtJWUnwxk8Bnppo3JeU3s6VH0HWuxGqsGQ2mpqfqIV3gWvKCkcCkccNA+Gep0sd195KxuVTIMGrU+nxW4j5eSqKF5JKSnB5KPTWpNt7bj3d2cKLQZr7sePOp6W1uNY5pAcJ6Z6ebVk2n25p+21GnU+mTpUtuU/5QpUnjkHiE4HEDp01mrsc/nbq/0Y9+M1pi7/AGytHmxrtvpyp1BM9MYyhHTw7rkhsJA8M49aPPpS9nzaGlbl06tSKrUp0RcF1ttAj8MKCkqJzyB97W3KVDRT6ZEhtqUtEdlDKVK8SEpABP1a6tGqJvr+aC7vo93Sm7EfuXub561+HrK1wSpCa9UgH3QPKXf/AFn9M6mtqZT6tzrSSp90g1aLkFZ6+yp08+3J/XLO/u5f+bWnS5ekCwtm7frlWYlPxW4MJoojJSV5U2kDoogfv0jritybuvcY3Tt1bEa34RbU5HmrKJJEbCnMJSFJ6gdPXfLjUhe1QPaRTDiWGHKc5Qyt6SaqrugtLuAnh3ZXkjgc5x4+fRNuGFc9pt7L02M6zeDCG6cuc6lKYpdjEKcUHASviQ2rB45ORkDVU9TLesRYmyalQnG4/sqv6Q6SUp6kDLfxa5O0Hu3bu4lBpUK36dPiPRZSnnDJZbQCkoI6FKj1zqtHaG5mtsRe4qFPFI8mEkMh9zvgkr4448cZz8evza/aW59xqTMqFDqEFlmK/wCTrTKfcSoq4hWRxSemCNLQlxh5YStQUCUkpJHn1pzaTtBWxaG3NJt+sU6sSZcVLiXVtNtqQrk4pQxyWD4KHiNL3tBbl0fcGpUiRbkWdCaiMONOpfQhsqKlAjHBRyMDz629Z/W1KMT4+RMfhp1L6NUTfX80F3fR7ulN2I/cvc3z1r8PSDrm1t9PVme61aVbU2uQ4pKhEVggqOD4alttds71gbhWxLmWtWGYzFTjOuuriqCUIDqSVE+YADOmV25P65Z393L/AM2tWrfX+y7SPm9N/wBKdIGzKtujGsWVGtZuqm1lB7vixCS41gj2TKykkdPHr01VLMvm47KdlOWvU1wFykpS8UtoXzCSSPbA++fDU023uDRpx3DRCqkZ1xRl/ldUQd2ovZBXkp44Vzx4Y9drVPZ4uut3htNWqjck9c6YiVIZS4pCUEIDKCBhIA8VH69ITss2ZQL0uisxLmp6Z0diCHW0FxaOKu8SM5SR5idcO8F5V6h1y4rCpVRWxaUR0xGaeEIUlDQIUE8yCs9fOVZ06OxP7hLg+kv+0jSe7NVo0O8tyKpTrlgJnQ24Lr6Wy4tGFh1ABykg+Cj9eq7vFa0em7x1i3LVpywyl9pmLEa5OKKlNIPEZJJJJOnD2dtlYVSp1bO5FqS230OtCL5X3sclJSrlx4kZ641qqHGahxGY0ZHBhlCW0JznCQMAdfiGvXRqib7fmgu76Pc1j/ZveWXtlTalDiUiPPEx5LxW6+pHHCcY6A6YXqsqp8FoH3tf8uj1WVU+C0D72v8Al0sN591pW57tJXLpbFPNPS6lPdPFznz4+OQMY4/v1oLfUj1LtH6jHk9N/wBKdJvand6XRLUbsVulRnY9SfXHVLU8QtAfIQSE4wcZzrn372gi7YRaK7Gq71QM9bqCHGUt8OAT1GCc55as9jX9I3NoVE2jlQmKfCkR24pqLThccAjp7wHgcDKu7APXpk+9qZqV0Pdn+Qqwacw1Wo1QSJypclRZWgvexlISnIIAbBz8emvs3srF2zrM+oRq0/UDKjiOUOMJbCfXBWcgn3tVu+OzZBuu7arXXrjlR1z3y8WkxUqCM+bJV11TqpcD3Zpki2qUy1XmqmgVFT8klhTZyW+ICc5HrM5+PSY2m3EkbdXTLrUWnsTXJEZcYtOulASFLSrOR/04/br8qm4kifu2i+1U9lElMtmX5IHSUZbSkAcvHrx/fpu+qxq+OlsU/wC9L/hrV1DmqqNGgTVoCFSY7bxSk5AKkg4B/brt0a85DDUhlbMhtDrSxxUhaQpKh7xB8dR3obon6npv3Vv+Gj0N0T9T037q3/DR6G6J+p6b91b/AIa/PQ3RP1PTfurf8Ndsmnw5MQRZMVh6MnADTjaVIGPD1pGOmuNFt0RtaVoo9NStJCgoRWwQR4EdNdVQpkGohAqEONKCMlAfaSvjnxxkHGvGJQqTDkIfiUuCw8j2rjUdCVDzdCBnX3No9NnPh+bT4ch4AAOOsJWoAdQMkZ13aNcM+kU2oOpcn0+JJcSOKVPMJWQPeBI8Nc3oYoX6lpn3Rv8Aho9DFC/UtM+6N/w0ehihfqWmfdG/5dSyEJbQlCEhKEjAAGAB72v3Ro0aNGjRo0aNGjRo0aNGjRr/2Q==" data-filename="VistaQR-Sito-web-sostieni_wikimedia_it_newsletter mtm_campaign=qrcode-2.jpeg">
                </td>
            </tr>
        </tbody>
    </table>

</div>


<footer class="letter-footer">
    <p style="text-align:center;font-size:7.0pt;">
        <strong>
            Scegli di destinare il 5 per mille a Wikimedia Italia scrivendo il codice fiscale 94039910156 nell'apposito riquadro.<br>
            Dai alla conoscenza libera un nuovo nome. Il tuo.
        </strong>
    </p>
    <table style="font-size:6.0pt; color:#4c4c4c; line-height: 1.6;" width="100%" cellspacing="1" cellpadding="2">
        <tbody>
            <tr>
                <td colspan="4" style="vertical-align:bottom;padding-bottom:0;">
                    <img style="margin-left:-3mm;width:156px;" src="https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/contribute/images/uploads/wikimedia-logo.png" class="note-float-left">
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding-top:0;font-family: 'Anonymous Pro', monospace;font-size:8.5pt">
                    <strong>Associazione per la diffusione della conoscenza libera</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Via Bergognone, 34<br>20144 Milano (MI)<br>Tel. 02 97677170
                </td>
                <td>
                    P. Iva 05599740965<br>Cod. Fisc. 94039910156<br>Codice SDI: KRRH6B9
                </td>
                <td>
                    https://wikimedia.it<br>segreteria@wikimedia.it<br>wikimediaitalia@pec.it
                </td>
                <td>
                    IBAN: IT03Y0306909606100000146060<br>BIC: BCITITMM<br>Intesa Sanpaolo
                </td>
            </tr>
        </tbody>
    </table>
</footer>