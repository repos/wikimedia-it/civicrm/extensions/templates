{literal}
    <style>
        @font-face {
          font-family: 'Anonymous Pro';
          font-style: normal;
          font-weight: normal;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/Anonymous-Pro-Regular.woff) format("woff");
        }
        @font-face {
          font-family: 'Montserrat';
          font-style: normal;
          font-weight: 700;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/Montserrat-Bold.ttf) format('truetype');
        }
        @font-face {
          font-family: 'Roboto Slab';
          font-style: normal;
          font-weight: normal;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/RobotoSlab-Regular.woff) format("woff");
        }
        @font-face {
          font-family: 'Roboto Slab';
          font-style: normal;
          font-weight: 700;
          src: url(https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/fonts/RobotoSlab-Bold.woff) format("woff");
        }


        @media print,
        dompdf {

            @page {
                size: 210mm 297mm;
                margin: 0;
            }

            body {
                font-size: 8pt;
                font-family: 'Roboto Slab', serif;
                font-weight: inherit;
            }

            p, table {
                font-family: 'Roboto Slab', serif;
                font-weight: inherit;
                line-height: 8pt;
            }

            .letter-header {
                margin-left:-17mm;
                margin-top:-10mm;
            }

            .letter-footer {
                position: fixed;
                left: 0;
                bottom: 0;
                width: 100%;
            }

            .letter-footer td {
                text-align: left;
                font-family: 'Roboto Slab', serif;
                font-weight: inherit;
            }
        }
    </style>
{/literal}
<div class="letter-body">
    <p class="letter-header">
        <img style="width: 210mm;" src="https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/contribute/images/uploads/header-202311.png">
        <br>
    </p>
    <p>Milano, {$smarty.now|date_format:"%d/%m/%Y"}</p>
    <p style="font-size:14pt;text-align:center;font-family: 'Montserrat', sans-serif;font-weight: bold;">
        RIEPILOGO DONAZIONI
    </p>
    <p>
        Wikimedia Italia<br>
        Associazione per la diffusione della conoscenza libera
    </p>
    <p style="font-size:14pt;text-align:center;font-family: 'Montserrat', sans-serif;font-weight: bold;">
        attesta che
    </p>
    <p>
        <strong>{contact.addressee}{if '{contact.custom_110}' != ""} ({contact.custom_110}){/if}</strong>
        ha effettuato le seguenti donazioni a titolo di erogazione liberale a Wikimedia Italia, Associazione di
        Promozione Sociale iscritta al Registro Unico Nazionale Enti Terzo Settore (RUNTS) con provvedimento dell’8
        marzo 2023.
    </p>
    <p>
        Solo le donazioni effettuate tramite bonifico bancario, carta di credito e assegno beneficiano delle
        agevolazioni fiscali previste dalla legge.
        Non è possibile detrarre le quote associative, che non sono per questo inserite in questa comunicazione.
    </p>

    <p>
        Wikimedia Italia<br>
        Associazione per la diffusione della conoscenza libera
    </p>
    <table width="100%" cellspacing="1" cellpadding="1" border="0">
        <tbody>
            <tr>
                <td>
                    {assign var=total value=0}
                    <table class="table" style="width:100%;border: 1px solid #808080;border-collapse:collapse;"
                        cellspacing="0" cellpadding="2" align="left">
                        <tbody>
                            <tr>
                                <th style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                    align="left">Data di pagamento</th>
                                <th style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                    align="center">Modalità di pagamento</th>
                                <th style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                    align="center">Tipo donazione</th>
                                <th style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;">
                                    Importo ricevuto €</th>
                            </tr>

                            {foreach from=$contributions item=contribution}
                                {assign var="date" value=$contribution.receive_date|date_format:"%d/%m/%Y"}
                                {assign var="payment" value=$contribution.payment_instrument}
                                {assign var="financial_type" value=$contribution.financial_type}
                                {assign var="total_amount" value=$contribution.total_amount|crmMoney}
                                {assign var=total value=$contribution.total_amount+$total}

                                <tr>
                                    <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                        align="left">{$date}</td>
                                    <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                        align="center">{$payment}</td>
                                    <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                        align="center">{$financial_type}</td>
                                    <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                        align="right">{$total_amount}</td>
                                </tr>

                            {/foreach}

                            <tr>
                                <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                    align="left"><strong>Totale</strong></td>
                                <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;">
                                </td>
                                <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;">
                                </td>
                                <td style="border: 1px solid #808080;border-collapse:collapse;padding:0 4px 0 4px;"
                                    align="right"><strong>{$total|crmMoney}</strong></td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

    <p>
        Si ricorda che ai fini della detrazione fiscale a questa attestazione vanno allegate le prove dei versamenti.
    </p>

</div>


<footer class="letter-footer">
    <p style="text-align:center;font-size:7.0pt;">
        <strong>
            Scegli di destinare il 5 per mille a Wikimedia Italia scrivendo il codice fiscale 94039910156 nell'apposito riquadro.<br>
            Dai alla conoscenza libera un nuovo nome. Il tuo.
        </strong>
    </p>
    <table style="font-size:6.0pt; color:#4c4c4c; line-height: 1.6;" width="100%"
        cellspacing="1" cellpadding="2">
        <tbody>
            <tr>
                <td colspan="4" style="vertical-align:bottom;padding-bottom:0;">
                    <img style="margin-left:-3mm;width:156px;" src="https://sostieni.wikimedia.it/sites/default/files/civicrm/persist/contribute/images/uploads/wikimedia-logo.png" class="note-float-left">
                </td>
            </tr>
            <tr>
                <td colspan="4" style="padding-top:0;font-family: 'Anonymous Pro', monospace;font-size:8.5pt">
                    <strong>Associazione per la diffusione della conoscenza libera</strong>
                </td>
            </tr>
            <tr>
                <td>
                    Via Bergognone, 34<br>20144 Milano (MI)<br>Tel. 02 97677170
                </td>
                <td>
                    P. Iva 05599740965<br>Cod. Fisc. 94039910156<br>Codice SDI: KRRH6B9
                </td>
                <td>
                    https://wikimedia.it<br>segreteria@wikimedia.it<br>wikimediaitalia@pec.it
                </td>
                <td>
                    IBAN: IT03Y0306909606100000146060<br>BIC: BCITITMM<br>Intesa Sanpaolo
                </td>
            </tr>
        </tbody>
    </table>
</footer>